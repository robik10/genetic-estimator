var express    = require('express'),        // call express
app = express(),                 // define our app using express
bodyParser = require('body-parser'),
path = require('path'),
request = require('request'),
q = require('q');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var port = process.env.PORT || 8090;
var credentials = {user: 'wtadmin', pass:'labcorp1'};

var estimation_url = 'http://dev2-phoenix.labcorp.com/phx-rest/patientresponsibility/estimate',
accountNumber = '',
testNumber = '';



// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); 
            
router.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

router.post('/ev', function(req, res) {      
    
       console.log('Request: '+ req.body); 
      var _req = getPayload(req.body);
        
       makeRequest( _req ).then(function( _res ){
            console.log('Responding back to UI.............');  
            res.status(200).send( _res );
       }, function(_errObj){
            console.log('Sending ERROR back to UI.............');  
            res.status( _errObj.status ).send({error: _errObj.error});
       });
         
});



/***
 * method that hits the estimate service to pull information
 * 
 */
function makeRequest(_request){    
    var _payLoad = _request;
    var _defer = q.defer();    
    var auth = "Basic " + new Buffer(credentials.user + ":" + credentials.pass).toString("base64");    
    debugger;
    console.log('Calling Service:: '+ JSON.stringify(_payLoad, null, 2)); 
    request({uri:estimation_url, headers:{"Authorization":auth, "Content-Type": "application/json", "Cookie": "SMCHALLENGE=YES"}, json:_payLoad, method:'POST'}, function (error, response, body) {
            debugger;
            console.log('In Service Call Back: '+response.statusCode);             
            if(error || response.statusCode >= 400 ){
                _defer.reject( {status: response.statusCode, error: error || response.body }  );
            };             
            if (!error && response.statusCode == 200) {
               _defer.resolve( response.body );
            }
        }
    );    
    
    return _defer.promise;
}



function getPayload(__REQ){
			var _requestPayLoad = {
                    "accountNumber":"31712450",
                    "testNumbers":["196527"],
                    "diagnosisCodes":["A08.19"],
                    "serviceDate":"2016-10-10",
                    "paymentType":"PI",
                    "labcorpPayerCd":__REQ.insurance,
                    "patient":{  
                        "firstName": __REQ.firstName,
                        "lastName": __REQ.lastName,
                        "gender":__REQ.gender,
                        "dateOfBirth": __REQ.dob
                    },
                    "patientInsurance":{  
                        "subscriberId":__REQ.memberID,
                        "providerNpi":"",
                        "relationshipToSubscriber":"SELF"
                    }
            };
            
            return _requestPayLoad;
}




// REGISTER OUR ROUTES -------------------------------
app.use(router);
app.use('/assets', express.static('assets'));






// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);